app.factory('localstorage',function($http, ngDialog, $location, $state){
    console.log('factory was called');
	var storage = {};
	storage.items = [];
    storage.categories = [];
    storage.pick_coords = false;
	
	  var initialized_items  = [ 
        {
            "name": "Savidor-Center",
            "address": "Tel Aviv",
            "coordinates": [32.083725,34.798195],
            "category": "Trains"
        },
        {
            "name": "Center HaShmona",
            "address": "Haifa",
            "coordinates": [32.822344, 34.997205],
            "category": "Trains"

        },
        {
            "name": "Kfar Saba Nordau",
            "address": "Kfar Saba",
            "coordinates": [32.168072, 34.916997],
            "category": "Trains"

        },
        {
            "name": "Gesher Theatre",
            "address": "Tel Aviv",
            "coordinates": [32.056373, 34.759924],
            "category": "Culture"
        },
        {
            "name": "Ganei Yehoshua",
            "address": "Tel Aviv",
            "coordinates": [32.101999, 34.815835], 
            "category": "Parks"
        },
        {
            "name": "Cinematheque",
            "address": "Tel Aviv",
            "coordinates": [32.070902, 34.783212], 
            "category": "Culture"
        },
    ];

    var init_categories = [
        "Trains","Sports","Culture","Parks","Nightlife"
    ]
    
        /* Set OnLoad Data from/initialized localstorage*/
    	if(localStorage.getItem('locations') != null) {
        	storage.items = JSON.parse(localStorage.getItem('locations'));
            // Set The Categories
            storage.categories = JSON.parse(localStorage.getItem('categories'));
            //localStorage.setItem('categories', JSON.stringify(storage.categories));
   		}
   		else {
	   		storage.items  = initialized_items;
            localStorage.setItem('locations', JSON.stringify(storage.items));
            // Set The Categories
            storage.categories = init_categories;
	        localStorage.setItem('categories', JSON.stringify(storage.categories));
	        console.log('first time running');
   		}

        /*
        * Add Item To The Storage
        */
        storage.addLocation = function(new_item) {
            if(storage.edit_mode) { // remove old one, push new, avoid ng-repad dup
                var index = storage.items.indexOf(storage.choice);
                console.log('index is: ' + index);
                storage.items.splice(index, 1); // Update the storage items
                storage.items.push(new_item);
                localStorage.setItem('locations', JSON.stringify(storage.items));
                console.log('new item pushed to storage: ' + new_item);


                storage.edit_mode = false;
                return;
            }

            storage.items.push(new_item);
            localStorage.setItem('locations', JSON.stringify(storage.items));
            console.log('new item pushed to storage: ' + new_item);
        }

        /*
        * Add Category To The Storage
        */
        storage.addCategory = function(new_cat) {
            if(storage.edit_mode) { // remove old one, push new, avoid ng-repad dup
                var index = storage.categories.indexOf(storage.cat_choice);
                console.log('index is: ' + index);
                storage.categories.splice(index, 1); // Update the storage items
                storage.categories.push(new_cat);
                localStorage.setItem('categories', JSON.stringify(storage.categories));
                console.log('new category pushed to storage: ' + new_cat);


                storage.edit_mode = false;
                $state.go('categories', {}, { reload: true });
                return;
            }

            storage.categories.push(new_cat);
            localStorage.setItem('categories', JSON.stringify(storage.categories));
            console.log('new cat pushed to storage: ' + new_cat);

            
        }

        /*
        * Set The Clicked Location
        */
        storage.setLocation = function(location) {
            //console.log('location came: ' + location.coordinates);
            storage.choice = location;
        }


        /*
        * Set Pick Coords Variable
        */
        storage.setPickCoords = function(demand) {
            console.log(demand);
            storage.pick_coords = demand;
        }

        /*
        * Erase The Local Storage
        */
        storage.eraseStorage = function() {
            console.log('inside factory erase');

            ngDialog.openConfirm({
            template:'\
                <h2>Erase Local Storage ?</h2>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
                </div>',
            plain: true
            }).then(function (success) {
                localStorage.removeItem("locations");
                storage.items = [];
                $state.reload();
                }, function (error) {
            });
    }

    /*
    * Remove Single Item  Function
    */
    storage.removeItem = function() { 
            if(storage.choice == undefined) {
                return;
            }

            ngDialog.openConfirm({
            template:'\
                <h2>Remove Location ?</h2>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
                </div>',
            plain: true
            }).then(function (success) {
                var index = storage.items.indexOf(storage.choice);
                console.log('index is: ' + index);
                storage.items.splice(index, 1); // Update the storage items
                localStorage.setItem('locations', JSON.stringify(storage.items)); // Reset The Items
                }, function (error) {
            });

    }

    /*
    * Remove Single Category  Function
    */
    storage.removeCategory = function() { 
            if(storage.cat_choice == undefined) {
                return;
            }

            ngDialog.openConfirm({
            template:'\
                <h2>Remove Category ?</h2>\
                <div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog(0)">No</button>\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm(1)">Yes</button>\
                </div>',
            plain: true
            }).then(function (success) {
                var index = storage.categories.indexOf(storage.cat_choice);
                console.log('catg index is: ' + index);
                storage.categories.splice(index, 1); // Update the storage items
                localStorage.setItem('categories', JSON.stringify(storage.categories)); // Reset The Items
                }, function (error) {
            });

    }

    return storage;
});