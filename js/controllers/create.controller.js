angular.module('app').controller('createController',['$scope','$state','localstorage','$location','ngDialog', function($scope,$state,localstorage,$location,ngDialog) {
   $scope.myForm = { }; // Controller form field 
   $scope.myForm.coordinates = [];
   $scope.categories = localstorage.categories; // set the categories for select input
   // If user picked coords, inject them
   if(localstorage.user_picked) {
      $scope.user_picked = true;
      $scope.myForm.coordinates = localstorage.choice.coordinates;
      console.log('user picked: ' + $scope.myForm.coordinates); // we need to indicate whether user really picked or not 
      localstorage.user_picked = false;
   }

   // if we are on edit_mode
   if(localstorage.edit_mode){
    console.log('edit mode indeed');
    $scope.myForm = localstorage.choice;
   }

   /*
   * Check for Location Existence 
   */ 
   var validate = function() {
   		for(var i = 0; i<localstorage.items.length;i++) {
   			/*console.log('name: ' + location.name);
   			console.log('name: ' + $scope.myForm.name);*/
  			if(localstorage.items[i].name == $scope.myForm.name) {
  				return true;
  			}
		}
		return false;
   }

   /*
   * Get Coordiantes From Map Function
   */
   $scope.getCoordinates = function() {
    console.log('inside get coordiantes');
    localstorage.setPickCoords(true);
    var point = {};
    point.coordinates = [32.074847, 34.792006];
    localstorage.setLocation(point);

    $location.path("/view");
   }


    /*
    * On Submit Form Click
    */
    $scope.addLocation = function() {
      if($scope.myForm.category == undefined) {
        ngDialog.open({
          template: '<h2>Category Empty</h2><p>Try Again</p>',
          plain: true
        }); return;
      }
    	if(validate() && (!localstorage.edit_mode) ){
    		ngDialog.open({
    			template: '<h2>Location Already Exists</h2><p>Try Again</p>',
    			plain: true
			});
    		return;
    	}
    	else {
    		// parse to float
    		$scope.myForm.coordinates[0] = parseFloat($scope.myForm.coordinates[0]);
    		$scope.myForm.coordinates[1] = parseFloat($scope.myForm.coordinates[1]);
    		localstorage.addLocation($scope.myForm);
    		ngDialog.open({
    			template: '<h2>Location Added Successfully</h2><p>Enjoy</p>',
    			plain: true
			});
			$location.path("/");


    	}
    	
        
    }

}]);
