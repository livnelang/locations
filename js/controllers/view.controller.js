angular.module("app").controller('viewController',['$scope','$state','localstorage','ngDialog','$location','$state', function($scope,$state,localstorage,ngDialog,$location,$state) {

    $scope.location = localstorage.choice;
    /* Check For A Null View */
    var checkLocation =  function() {
      if($scope.location == null) {
        return false;
      }
      return true;
    }
      
      /*
      * Used To Initialize The Map
      */
      var map;
      function initMap() {
          var center_point = new google.maps.LatLng($scope.location.coordinates[0],$scope.location.coordinates[1]);
          var d = $( "#googleMap" );
        map = new google.maps.Map(d[0], {
          center: center_point,
          zoom: 13
        });
        /* A New Marker */
        if(!localstorage.pick_coords) {
          var marker = new google.maps.Marker({
            position:center_point,
          });
          marker.setMap(map);
        }
  

  /* Set A Event Listener For The Map */
  google.maps.event.addListener(map, 'click', function (e) {
        //alert("Latitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());
        localstorage.setLocation({ "coordinates" : [e.latLng.lat().toFixed(3), e.latLng.lng().toFixed(3)] });
        localstorage.setPickCoords(false);
        //console.log('after picking: ' + localstorage.pick_coords);
        localstorage.user_picked = true;
        $state.go("add-location");

      });
}

// A $( document ).ready() block.
$( document ).ready(function() {
    console.log( "ready!" );
    if(checkLocation() ) {
      initMap();
    }
    else {
      // open dialog and state back
      ngDialog.open({
            template: '<h2>Location View</h2><p>Please select a location and try again</p>',
            plain: true
        });
      $location.path("/locations");
      } 
  });
}]);
