/******************************
 *
 * Bottom Navbar directive
 *
 ******************************/
app.directive('bottomBar',['localstorage', function(localstorage){
    return {
        restrict: 'E',
        replace: true,
        scope:{
        },
        templateUrl: 'templates/bottom-bar.html',
         link: function($scope, element, attrs) {
                $scope.erase = function() {
                    console.log('inside directrive erase');
                    localstorage.eraseStorage();
                }
        }
    };
}]);

/******************************
 *
 * Top Navbar directive
 *
 ******************************/
app.directive('topBar',['localstorage','$location','$state', function(localstorage,$location,$state){
    return {
        restrict: 'E',
        replace: true,
        scope:{
        },
        templateUrl: 'templates/top-bar.html',
        link: function($scope, element, attrs) {
                $scope.removeItem = function() {
                    if(localstorage.object_type == 'location') {
                        console.log('inside directive remove location');
                        localstorage.removeItem();
                        //localstorage.eraseStorage();
                    }
                    else {
                        console.log('inside directive remove category');
                        localstorage.removeCategory();
                    }
                    
                }
                /* view function resolver */
                $scope.view = function() {
                    if(localstorage.object_type == 'location') {
                        $location.path("/view");
                        console.log('inside directive view resolver');
                        //localstorage.removeItem();
                        //localstorage.eraseStorage();
                    }
                    else {
                        //console.log('cat in directive: ' + cat);
                        console.log('inside directive view resolver');
                        $location.path('/locations');
                        //localstorage.removeCategory();
                    }
                    
                }
                /* edit function resolver */
                $scope.edit = function() {
                    
                    
                    
                    if(localstorage.object_type == 'location') {
                        if(localstorage.choice == null) {
                            console.log('null edit');
                            return;
                        }
                        localstorage.edit_mode = true; // turn off in own controller
                        $state.go("add-location");
                        //console.log('inside directive edit resolver');
                    }
                    else {
                        if(localstorage.cat_choice == null) {
                            console.log('null edit');
                            return;
                        }
                        localstorage.edit_mode = true; // turn off in own controller
                        console.log('inside directive edit resolver');
                        $state.go('categories', {}, { reload: true });
                        //localstorage.removeCategory();
                    }
                    
                }
        }
    };
}]);

/******************************
 *
 * Locations List directive
 *
 ******************************/
app.directive('locationsList',function(){
    return {
        restrict: 'E',
        replace: true,
        scope:{
        },
        templateUrl: 'templates/locations-list.html',
        compile: function(tElem,attrs) {
            return function(scope,elem,attrs) {
            };
        }
    };
});

/******************************
 *
 * Map View Location directive
 *
 ******************************/
app.directive('locationView',function(){
    return {
        restrict: 'E',
        replace: true,
        scope:{
        },
        templateUrl: 'templates/location-view.html',
        compile: function(tElem,attrs) {
            return function(scope,elem,attrs) {
            };
        }
    };
});