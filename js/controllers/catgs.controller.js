angular.module("app").controller('catgsController',['$scope','$state','localstorage','vibrator', function($scope,$state,localstorage,vibrator) {
   //console.log()
   localstorage.object_type = 'category';
   //localStorage.removeItem("locations");
   //$scope.new_category = null;
   $scope.cat_signed = {};
   $scope.categories = localstorage.categories;
   $scope.edit = localstorage.edit_mode;
   if($scope.edit && localstorage.cat_choice != null) {
      $scope.new_category = localstorage.cat_choice;
   }
   else { // could be true, but localstorage cat is null
      $scope.edit = false;
      localstorage.edit_mode = false;
   }

   /*
   * On Category Tap Vibrate
   */

   $scope.vibrate = function($event, categ) {
     vibrator.vibrate(200);
     localstorage.cat_choice = categ; // set the current category

     /* If We Already Selected One */
     if($scope.cat_signed) {
        $($scope.cat_signed.element).removeClass("tapped_back");
     }
     /* Replace With the second One */
     $scope.cat_signed.category = categ;
     $scope.cat_signed.element = $($event.target);
     $($scope.cat_signed.element).addClass("tapped_back");
     // Send to factory selected location
     //localstorage.setLocation($scope.choice.location);
   };

   /*
   * Add Category Fucntion
   */
   $scope.addCategory = function() {
      //console.log('new cat in ctrl: ' + $scope.new_category);
      localstorage.addCategory($scope.new_category);
   }
               
}]);
