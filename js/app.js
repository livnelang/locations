var app = angular.module("app", ["ui.router","ngTouch","ngDialog","angular-vibrator"]);

app.config(function($stateProvider, $urlRouterProvider)  {
 
  $urlRouterProvider.otherwise("/");
 
  $stateProvider
    .state('locations', {
      url: '/?cat',
      templateUrl: 'templates/locations-list.html',
      controller: 'locationsController',
    })

    .state('add-location', {
      url: '/add',
      templateUrl: 'templates/create-location.html',
      controller: 'createController',
    })

    .state('view', {
      url: '/view',
      templateUrl: 'templates/location-view.html',
      controller: 'viewController',
    })

    .state('categories', {
      url: '/categories',
      templateUrl: 'templates/categories.html',
      controller: 'catgsController',
    })

});
