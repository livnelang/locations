angular.module("app").controller('locationsController',['$scope','$state','localstorage','vibrator','$stateParams', function($scope,$state,localstorage,vibrator,$stateParams) {
   //localStorage.removeItem("locations");
   console.log('loaded');
   localstorage.object_type = 'location';
   $scope.locations = localstorage.items;
   $scope.choice = {};

   // Check whether state params has filtered category
   if(localstorage.cat_choice != undefined) {
      console.log('category came: ' + localstorage.cat_choice);
      $scope.locations = [];
      // filter the items
      angular.forEach(localstorage.items, function(loc, key) {
        if(loc.category == localstorage.cat_choice) {
            $scope.locations.push(loc);
        }
      });
      localstorage.cat_choice = null;
   }

   /*
   * On Location Tap Vibrate
   */
   $scope.vibrate = function($event, loc) {
     vibrator.vibrate(200);
     /* If We Already Selected One */
     if($scope.choice.element) {
        $($scope.choice.element).removeClass("tapped_back");
     }
     /* Check pick_coords variable */
     if(localstorage.pick_coords) {
        localstorage.setPickCoords(false);
     }
     /* Replace With the second One */
     $scope.choice.location = loc;
     $scope.choice.element = $($event.target).parent();
     $($scope.choice.element).addClass("tapped_back");
     // Send to factory selected location
     localstorage.setLocation($scope.choice.location);
   };
}]);
